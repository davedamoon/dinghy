module gitlab.com/davedamoon/dinghy/backend

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.69
	github.com/codahale/hdrhistogram v0.0.0-20161010025455-3a0bb77429bd // indirect
	github.com/disintegration/imaging v1.6.2
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/websocket v1.4.2
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/mholt/archiver/v3 v3.5.0
	github.com/opentracing-contrib/go-grpc v0.0.0-20210225150812-73cb765af46e
	github.com/opentracing/opentracing-go v1.2.0
	github.com/prometheus/client_golang v1.11.0
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
	github.com/urfave/cli/v2 v2.3.0
	go.uber.org/atomic v1.6.0 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.27.1
)
